<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Enroll Now - Matrix</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body class="contact-us">
	<div id="main_wrapper">
		<?php require('header.php'); ?>
		<?php require('navbar-top.php'); ?>	
		<div class="main-page container">
			<div class="row">
				<div class="col-xs-12">
					<hr style="border-top: 10px solid #00451E;margin-top: 0;">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="banner">
						<img src="img/banner.jpg" alt="" class="img-responsive" style="width: 100%;">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="bg-green">
						<h1>Enroll Now</h1>
					</div>				
				</div>
			</div>

		</div>
		<?php require('footer.php'); ?>
	</div>
</body>
</html>