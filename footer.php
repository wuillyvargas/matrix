<div id="footer" class="container">
	<div class="row">
		<div class="col-xs-12">
			<hr style="border-top: 10px solid #00451E;margin-top: 0;">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6" style="padding-right: 0;">
			<div class="footer-left">
				<h3 style="margin-bottom: 40px;">Matrix 4 success Academy</h3>
				<h4>Home</h4>
				<h4>Enroll Now</h4>
				<h3 style="margin-top: 40px;">1046 E. 34st, Los Angeles, CA 90011</h3>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6" style="padding-left: 0;">
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3307.2230462181215!2d-118.25946488492849!3d34.012485227271924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c86346585287%3A0xd6da0dc6e006eba0!2s1046+E+34th+St%2C+Los+Angeles%2C+CA+90011%2C+USA!5e0!3m2!1sen!2sve!4v1533246336324" width="100%" height="248" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
<!-- JavaScript -->
<script type="text/javascript" src="js\jquery.js"></script>
<script type="text/javascript" src="js\jquery.cookie.pack.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="js\jquery-migrate.min.js"></script>
<script type="text/javascript" src="js\jquery.fancybox.js"></script>
<script type="text/javascript" src="js\jquery.elastic.source.js"></script>
<script type="text/javascript" src="js\jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript" src="js\jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="js\jquery.ui.totop.js"></script>
<script type="text/javascript" src="js\jquery.validate.min.js"></script>
<script type="text/javascript" src="js\login-with-ajax.js"></script>
<script type="text/javascript" src="js\bootstrap-button.js"></script>
<script type="text/javascript" src="js\bootstrap-carousel.js"></script>
<script type="text/javascript" src="js\bootstrap-collapse.js"></script>
<script type="text/javascript" src="js\bootstrap-modal.js"></script>
<script type="text/javascript" src="js\bootstrap-tab.js"></script>
<script type="text/javascript" src="js\bootstrap-tooltip.js"></script>
<script type="text/javascript" src="js\bootstrap-transition.js"></script>
<script type="text/javascript" src="js\bootstrap-popover.js"></script>
<script type="text/javascript" src="js\easing.js"></script>
<script type="text/javascript" src="js\global.js"></script>
<script type="text/javascript" src="js\imagescale.js"></script>
<script type="text/javascript" src="js\login-with-ajax.source.js"></script>
<script type="text/javascript" src="js\main.js"></script>
<script type="text/javascript" src="js\theme.min.js"></script>
<script type="text/javascript" src="js\tinymce.min.js"></script>
<script type="text/javascript" src="js\transit.js"></script>
<script type="text/javascript" src="js\admin.js"></script>
<script type="text/javascript" src="js\greensock.js"></script>
<script type="text/javascript" src="js\layerslider.transitions.js"></script>
<script type="text/javascript" src="js\layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript" src="js\tabs.js"></script>
<script type="text/javascript" src="js\ticker.js"></script>
<script type="text/javascript" async
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML">
</script>