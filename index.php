<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Home - Matrix</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body class="home">
	<div id="main_wrapper">
	<?php require('header.php'); ?>
	<?php require('navbar-top.php'); ?>	
	<div class="main-page container">
		<div class="row">
			<div class="col-xs-12">
				<hr style="border-top: 10px solid #00451E;margin-top: 0;">
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="banner">
					<img src="img/banner.jpg" alt="" class="img-responsive" style="width: 100%;">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="bg-green">
					<h1>Is Time or Being Short of Credits Preventing You <br>from Earning a High School Diploma?</h1>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6" style="padding-right: 0;">
				<div class="brown">
					<p>Matrix academy provides a fexible learning environment with a curriculum that is tailored to each student’s strengths, needs, and interests.</p>
				</div>
				<div class="offer-box">
					<h2 class="offer">WE OFFER</h2>
					<ul class="list-offer">
						<li>College Courses </li>
						<li>On-Site & Online Support </li>
						<li>Independent Studies </li>
						<li>Access to Your Own Laptop </li>
						<li>Flexible Scheduling </li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6" style="padding-left: 0;">
				<div class="lightbrown">
					<h3>ANY STUDENT.  ANYTIME.  ANYWHERE</h3>
					<p><strong>Personalization</strong> allows the student <strong>to</strong>
					take <strong>ownership</strong> of their education by
					envisioning, constructing, and committing
					to a set of both educational and personal
					goals.</p>
					<p><strong>Our Mastery-based Learning program</strong>
					marries Common Core State Standards
					with student interest is driven curriculum
					and results in rigorous coursework</p>
					<p><strong>Specifically designed to promote
					problem-solving</strong> and critical thinking
					skills that are tailored around individual
					student needs</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="home-footer">
					<p>Make your next move your best move with Matrix.</p>
					<h2>Enroll Today!</h2>
					<h3>1046 E. 34st, Los Angeles, CA 90011</h3>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>
</html>