<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Contact - Matrix</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body class="contact-us">
	<div id="main_wrapper">
		<?php require('header.php'); ?>
		<?php require('navbar-top.php'); ?>	
		<div class="main-page container">
			<div class="row">
				<div class="col-xs-12">
					<hr style="border-top: 10px solid #00451E;margin-top: 0;">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="banner">
						<img src="img/banner.jpg" alt="" class="img-responsive" style="width: 100%;">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="bg-green">
						<h1>Contact Us</h1>
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
					<form class="form-horizontal" action="/action_page.php">
						<div class="form-group">
							<label class="control-label col-sm-2" for="name">Name:</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="email">Email:</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="email">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="phone">Phone:</label>
							<div class="col-sm-10">
								<input type="tel" class="form-control" id="phone">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2" for="comment">Comment:</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="5" id="comment"></textarea>
							</div>
						</div>
						<div class="form-group"> 
							<div class="col-sm-offset-2 col-sm-10 text-right">
								<button type="submit" class="btn btn-default">Submit</button>
								<button type="submit" class="btn btn-default">Clear</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php require('footer.php'); ?>
	</div>
</body>
</html>